//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "pv_display_resource.h"
#include "pv_common.h"
#include <unistd.h>
void
pv_display_resource_t::dirty_rectangle_request(
    struct pv_display_backend *display, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{

    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self || !self->m_valid) {
        return;
    }

    QMetaObject::invokeMethod(self, "add_dirty_rectangle", Qt::QueuedConnection, Q_ARG(uint32_t, x), Q_ARG(uint32_t, y), Q_ARG(uint32_t, w), Q_ARG(uint32_t, h));
}

void
pv_display_resource_t::move_cursor_request(struct pv_display_backend *display, uint32_t x, uint32_t y)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self || !self->m_valid) {
        return;
    }

    QMetaObject::invokeMethod(self, "move_cursor", Qt::QueuedConnection, Q_ARG(uint32_t, self->m_key), Q_ARG(uint32_t, x), Q_ARG(uint32_t, y));
}

void
pv_display_resource_t::update_cursor_request(struct pv_display_backend *display,
                                             uint32_t xhot,
                                             uint32_t yhot,
                                             uint32_t show)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self || !self->m_valid) {
        return;
    }

    QMetaObject::invokeMethod(self, "update_cursor", Qt::QueuedConnection, Q_ARG(uint32_t, xhot), Q_ARG(uint32_t, yhot), Q_ARG(uint32_t, show));
}

void
pv_display_resource_t::set_display_request(struct pv_display_backend *display,
                                           uint32_t w,
                                           uint32_t h,
                                           uint32_t stride)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self) {
        return;
    }

    if ((w == 0 || h == 0 || stride == 0) || (w > 4096 || h > 4096 || stride > 4096 * 4)) {
        vg_debug() << "Bad set_display request" << w << h << stride;
        return;
    }

    QMetaObject::invokeMethod(self, "set_display", Qt::QueuedConnection, Q_ARG(uint32_t, w), Q_ARG(uint32_t, h), Q_ARG(uint32_t, stride));
}

void
pv_display_resource_t::blank_display_request(struct pv_display_backend *display, uint32_t reason)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self || !self->m_valid) {
        return;
    }

    QMetaObject::invokeMethod(self, "blank_display", Qt::QueuedConnection, Q_ARG(uint32_t, reason));
}

void
pv_display_resource_t::error_handler(struct pv_display_backend *display)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(display->get_driver_data(display));

    if (!self) {
        return;
    }

    //    QMetaObject::invokeMethod(self, "disconnect_display", Qt::QueuedConnection);
}

void
pv_display_resource_t::new_dirty_rect_connection(void *opaque, struct libivc_client *client)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(opaque);

    if (!self) {
        return;
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self,
                              "finish_new_dirty_rect_connection",
                              Qt::QueuedConnection,
                              Q_ARG(void *, (void *) client));
}

void
pv_display_resource_t::new_framebuffer_connection(void *opaque, struct libivc_client *client)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(opaque);

    if (!self) {
        return;
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "finish_new_framebuffer_connection", Qt::QueuedConnection, Q_ARG(void *, (void *) client));
}

void
pv_display_resource_t::new_cursor_image_connection(void *opaque, struct libivc_client *client)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(opaque);

    if (!self) {
        return;
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "finish_new_cursor_image_connection", Qt::QueuedConnection, Q_ARG(void *, (void *) client));
}

void
pv_display_resource_t::new_event_connection(void *opaque, struct libivc_client *client)
{
    pv_display_resource_t *self = static_cast<pv_display_resource_t *>(opaque);

    if (!self) {
        return;
    }

    QMutexLocker locker(&self->m_lock);

    QMetaObject::invokeMethod(self, "finish_new_event_connection", Qt::QueuedConnection, Q_ARG(void *, (void *) client));
}

void
pv_display_resource_t::finish_new_dirty_rect_connection(void *cli)
{
    if (!m_pv_display) {
        return;
    }

    struct libivc_client *client = (struct libivc_client *) cli;

    m_pv_display->finish_dirty_rect_connection(m_pv_display.get(), client);
}

void
pv_display_resource_t::finish_new_framebuffer_connection(void *cli)
{
    if (!m_pv_display) {
        return;
    }

    struct libivc_client *client = (struct libivc_client *) cli;

    m_pv_display->finish_framebuffer_connection(m_pv_display.get(), client);
}

void
pv_display_resource_t::finish_new_cursor_image_connection(void *cli)
{
    if (!m_pv_display) {
        return;
    }

    struct libivc_client *client = (struct libivc_client *) cli;

    m_pv_display->finish_cursor_connection(m_pv_display.get(), client);
}

void
pv_display_resource_t::finish_new_event_connection(void *cli)
{
    if (!m_pv_display) {
        return;
    }

    struct libivc_client *client = (struct libivc_client *) cli;

    m_pv_display->finish_event_connection(m_pv_display.get(), client);
}

pv_display_resource_t::pv_display_resource_t(std::shared_ptr<pv_display_consumer> consumer,
                                             uint32_t key,
                                             uint32_t x,
                                             uint32_t y,
                                             uint32_t w,
                                             uint32_t h,
                                             qlist_t<uint32_t> port_list,
                                             std::shared_ptr<plane_t> parent) : render_source_plane_t(rect_t(0, 0, w, h), point_t(x, y), parent),
                                                                                m_x(x),
                                                                                m_y(y)
{
    int rc = 0;
    (void) w;
    (void) h;
    QMutexLocker locker(&m_lock);
    m_consumer = consumer;
    m_key = key;

    Expects(port_list.size() == 4);

    struct pv_display_backend *display = nullptr;
    rc = consumer->create_pv_display_backend(consumer.get(),
                                             &display,
                                             consumer->rx_domain,
                                             port_list[0],
                                             port_list[1],
                                             port_list[2],
                                             port_list[3],
                                             (void *) this);

    for (int i = 0; i < 4; i++) {
        m_port_list.push_back(port_list[i]);
    }

    m_pv_display = std::shared_ptr<pv_display_backend>(display, [](struct pv_display_backend *ptr) { (void) ptr; });

    if (rc || !m_pv_display) {
        vg_debug() << "Create pv backend failed: " << rc << " display ptr: " << m_pv_display.get();
        throw std::logic_error("Failed to create display");
    }

    // Connection callbacks
    m_pv_display->register_dirty_rect_connection_handler(m_pv_display.get(),
                                                         &pv_display_resource_t::new_dirty_rect_connection);
    m_pv_display->register_framebuffer_connection_handler(m_pv_display.get(),
                                                          &pv_display_resource_t::new_framebuffer_connection);
    m_pv_display->register_cursor_image_connection_handler(m_pv_display.get(),
                                                           &pv_display_resource_t::new_cursor_image_connection);
    m_pv_display->register_event_connection_handler(m_pv_display.get(),
                                                    &pv_display_resource_t::new_event_connection);

    // Packet callbacks
    m_pv_display->register_dirty_rectangle_handler(m_pv_display.get(),
                                                   &pv_display_resource_t::dirty_rectangle_request);
    m_pv_display->register_move_cursor_handler(m_pv_display.get(),
                                               &pv_display_resource_t::move_cursor_request);
    m_pv_display->register_update_cursor_handler(m_pv_display.get(),
                                                 &pv_display_resource_t::update_cursor_request);
    m_pv_display->register_set_display_handler(m_pv_display.get(),
                                               &pv_display_resource_t::set_display_request);
    m_pv_display->register_blank_display_handler(m_pv_display.get(),
                                                 &pv_display_resource_t::blank_display_request);
    m_pv_display->register_fatal_error_handler(m_pv_display.get(),
                                               &pv_display_resource_t::error_handler);

    m_pv_display->start_servers(m_pv_display.get());

    QMetaObject::invokeMethod(this, "issue_add_display_call", Qt::QueuedConnection, Q_ARG(uint32_t, port_list[0]), Q_ARG(uint32_t, port_list[1]), Q_ARG(uint32_t, port_list[2]), Q_ARG(uint32_t, port_list[3]));
}

pv_display_resource_t::~pv_display_resource_t()
{
    QMutexLocker locker(&m_lock);

    if (m_cursor) {
        m_cursor->set_cursor_image(nullptr);
    }

    if (m_pv_display) {
        m_pv_display->set_driver_data(m_pv_display.get(), nullptr);
    }

    if (m_framebuffer) {
        m_framebuffer = nullptr;
    }

    m_pv_display->disconnect_display(m_pv_display.get());
    m_consumer->destroy_display(m_consumer.get(), m_pv_display.get());

    emit remove_display_signal(m_pv_display, m_key, m_port_list);
}

void
pv_display_resource_t::send_add_display()
{
    m_consumer->add_display(m_consumer.get(),
                            m_key,
                            m_port_list[0],
                            m_port_list[1],
                            m_port_list[2],
                            m_port_list[3]);
}

void
pv_display_resource_t::issue_add_display_call(uint32_t port0,
                                              uint32_t port1,
                                              uint32_t port2,
                                              uint32_t port3)
{
    // Notify the front end that the backend is ready.
    m_consumer->add_display(m_consumer.get(),
                            m_key,
                            port0,
                            port1,
                            port2,
                            port3);
}

qlist_t<uint32_t>
pv_display_resource_t::port_list()
{
    return m_port_list;
}

void
pv_display_resource_t::disconnect_display()
{
    QMutexLocker locker(&m_lock);
    if (display()) {
        display()->disconnect_display(display());
        m_valid = false;
        m_framebuffer = nullptr;
    }
}

void
pv_display_resource_t::add_dirty_rectangle(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
    emit dirty_rectangle_signal(rect_t(x, y, w, h));
}

void
pv_display_resource_t::move_cursor(uint32_t key, uint32_t x, uint32_t y)
{
    if (m_cursor_visible) {
        emit move_cursor_signal(key, point_t(x, y));
    }
}

void
pv_display_resource_t::update_cursor(uint32_t x_hot, uint32_t y_hot, uint32_t show)
{
    m_cursor_visible = (show != 0);

    if (m_cursor_visible) {
        std::shared_ptr<QImage> cursor_image = std::make_shared<QImage>((unsigned char *) m_pv_display->cursor.image,
                                                                        64,
                                                                        64,
                                                                        64 * 4,
                                                                        QImage::Format_RGB32);

        if (cursor_image || !cursor_image->isNull()) {
            m_cursor = std::make_shared<cursor_t>(point_t(x_hot, y_hot));
            m_cursor->set_cursor_image(cursor_image);
            emit update_cursor_signal(m_key, point_t(x_hot, y_hot), m_cursor);
        } else {
            vg_debug() << "Created cursor for " << m_key << " is NULL!";
            QMetaObject::invokeMethod(this, "update_cursor", Qt::QueuedConnection, Q_ARG(uint32_t, x_hot), Q_ARG(uint32_t, y_hot), Q_ARG(uint32_t, show));
            return;
        }
    } else {
        emit hide_cursor_signal(m_key);
    }
}

void
pv_display_resource_t::set_display(uint32_t w, uint32_t h, uint32_t stride)
{
    QMutexLocker locker(&m_lock);
    if (!m_framebuffer || (m_framebuffer->bytesPerLine() != (int32_t) stride || m_framebuffer->width() != (int32_t) w || m_framebuffer->height() != (int32_t) h)) {
        m_framebuffer = std::make_shared<QImage>((unsigned char *) m_pv_display->framebuffer,
                                                 w,
                                                 h,
                                                 stride,
                                                 QImage::Format_RGB32);

        if (m_framebuffer && !m_framebuffer->isNull()) {
            m_valid = true;
            vg_debug() << "Creating framebuffer" << w << h << stride << m_valid << m_x << m_y << m_key;
            this->set_rect(rect_t(0, 0, w, h));
            emit dirty_rectangle_signal(rect_t(0, 0, w, h));
            emit render_source_plane_signal(m_key);
            m_retry_width = m_retry_height = m_retry_stride = 0;
            return;
        }

        vg_debug() << "Created framebuffer for " << m_key << " is NULL!";
        m_framebuffer = nullptr;
        m_valid = false;
        m_retry_width = w;
        m_retry_height = h;
        m_retry_stride = stride;
        QTimer::singleShot(1000, this, SLOT(retry_set_display()));
    }
}

void
pv_display_resource_t::retry_set_display()
{
    QMutexLocker locker(&m_lock);

    //Do we still need to retry to create the framebuffer?
    if (!m_retry_width || !m_retry_height || !m_retry_stride)
        return;

    QMetaObject::invokeMethod(this, "set_display", Qt::QueuedConnection, Q_ARG(uint32_t, m_retry_width), Q_ARG(uint32_t, m_retry_height), Q_ARG(uint32_t, m_retry_stride));
    vg_debug() << "retried the framebuffer";
}

void
pv_display_resource_t::blank_display(uint32_t reason)
{
    emit blank_display_signal(reason == 0);
}

render_command_t
pv_display_resource_t::render_command()
{
    return COPYBLIT;
}

void
pv_display_resource_t::clear()
{
}

bool
pv_display_resource_t::update()
{
    return true;
}

bool
pv_display_resource_t::update_cached()
{
    return true;
}

bool
pv_display_resource_t::requires_update() const
{
    return true;
}

struct pv_display_backend *
pv_display_resource_t::display()
{
    return m_pv_display.get();
}

bool
pv_display_resource_t::valid()
{
    return (m_pv_display && m_pv_display->framebuffer_size != 0);
}
