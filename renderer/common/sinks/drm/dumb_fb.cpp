//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#define _FILE_OFFSET_BITS 64

#include <QDebug>

#include "dumb_fb.h"

#include <errno.h>
#include <exception>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gsl/gsl>

extern "C" {
#include <drm/i915_drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
}

dumb_fb_t::dumb_fb_t(int32_t fd, const QSize &size, QImage::Format format)
    : framebuffer_t(size), m_drm_fd(fd), m_size(0)
{
    Expects(fd > 0);

    if (size == QSize(0, 0))
        return;

    Expects(size.width() > 0);
    Expects(size.height() > 0);

    // Check the DRM device's information
    std::unique_ptr<drmVersion, void (*)(drmVersion *)> version(drmGetVersion(m_drm_fd), drmFreeVersion);

    if (QString(version->name) == "vmwgfx")
        m_vendor = DEVICE_VENDOR::VMWARE;

    if (QString(version->name) == "i915")
        m_vendor = DEVICE_VENDOR::INTEL_I915;

    // ------------------------------------------------------------------------
    // Allocate
    struct drm_mode_create_dumb create_arg = {};
    create_arg.width = width();
    create_arg.height = height();
    create_arg.bpp = 32;

    if (drmIoctl(fd, DRM_IOCTL_MODE_CREATE_DUMB, &create_arg)) {
        auto error = errno;
        throw std::system_error(error, std::system_category(), "Failed to create DRM dumb buffer");
    }

    m_size = create_arg.size;
    m_stride = create_arg.pitch;
    m_handle = create_arg.handle;

    // ------------------------------------------------------------------------
    // Map

    switch (m_vendor) {
        case DEVICE_VENDOR::INTEL_I915: {
#ifdef I915_PARAM_MMAP_VERSION
            int value = 0;
            struct drm_i915_getparam getparam_arg = {};
            getparam_arg.param = I915_PARAM_MMAP_VERSION;
            getparam_arg.value = &value;

            if (drmIoctl(fd, DRM_IOCTL_I915_GETPARAM, &getparam_arg) == 0) {
                struct drm_i915_gem_mmap map_arg = {};
                map_arg.handle = m_handle;
                map_arg.size = m_size;
                map_arg.flags = I915_MMAP_WC;

                if (drmIoctl(fd, DRM_IOCTL_I915_GEM_MMAP, &map_arg)) {
                    auto error = errno;
                    throw std::system_error(error, std::system_category(), "DRM_IOCTL_I915_GEM_MMAP failed");
                }

                m_vaddr = (uint8_t *) map_arg.addr_ptr;
                break;
            }
#endif
#if defined(__GNUC__) && __GNUC__ >= 7
            [[gnu::fallthrough]];
#endif
        }

        default: {
            struct drm_mode_map_dumb map_arg = {};
            map_arg.handle = m_handle;

            if (drmIoctl(fd, DRM_IOCTL_MODE_MAP_DUMB, &map_arg)) {
                throw std::runtime_error("Failed to set up DRM dumb buffer mmap");
            }

            if ((m_vaddr = (uint8_t *) mmap(0,
                                            m_size,
                                            PROT_READ | PROT_WRITE,
                                            MAP_SHARED,
                                            fd,
                                            map_arg.offset)) == MAP_FAILED) {
                auto error = errno;
                m_vaddr = NULL;
                throw std::system_error(error, std::system_category(), "Failed to mmap DRM dumb buffer");
            }

            break;
        }
    }

    // ------------------------------------------------------------------------
    // Add to DRM FB
    if (drmModeAddFB(fd, m_width, m_height, 24, 32, m_stride, m_handle, &m_unique_id)) {
        throw std::runtime_error("Failed to add drm framebuffer to dumb buffer object");
    }

    m_image = std::make_shared<QImage>((unsigned char *) m_vaddr, m_width, m_height, m_stride, format);

    if (m_image->isNull()) {
        throw std::runtime_error("Failed to construct QImage from dumb buffer");
    }

    clear();

    return;
}

dumb_fb_t::~dumb_fb_t()
{
    if (m_drm_fd > 0 && m_unique_id > 0) {
        drmModeRmFB(m_drm_fd, m_unique_id);
    }

    if (m_vaddr != NULL && m_size > 0) {
        munmap(m_vaddr, m_size);
    }

    if (m_drm_fd > 0 && m_handle > 0) {
        drmIoctl(m_drm_fd, DRM_IOCTL_MODE_DESTROY_DUMB, &m_handle);
    }

    m_unique_id = 0;
    m_size = 0;
    m_width = 0;
    m_height = 0;
    m_stride = 0;
    m_handle = 0;

    m_vaddr = NULL;
    m_image = NULL;

    return;
}

int32_t
dumb_fb_t::fd(void) const
{
    return m_drm_fd;
}

void
dumb_fb_t::clear()
{
    switch (m_image->format()) {
        case QImage::Format_ARGB32:
            m_image->fill(Qt::transparent);
            break;

        default:
            m_image->fill(QColor(Qt::black));
    }

    flush(m_image->rect());

    return;
}

void
dumb_fb_t::flush(const region_t &region)
{
    switch (m_vendor) {
        case DEVICE_VENDOR::VMWARE: {
            (void) region;
            if (drmModeDirtyFB(m_drm_fd, m_unique_id, NULL, 0)) {
                auto error = errno;
                throw std::system_error(
                    error, std::system_category(), "Failed to queue dirty rect to DRM driver");
            }

            break;
        }
        case DEVICE_VENDOR::UNKNOWN:
        case DEVICE_VENDOR::INTEL_I915:
        case DEVICE_VENDOR::AMD:
        case DEVICE_VENDOR::NVIDIA:
        default:
            break;
    }

    return;
}
