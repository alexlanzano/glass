//
// backlight_t
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <algorithm>   // std::min, std::max
#include <fcntl.h>     // open
#include <libudev.h>   // udev_new,
#include <math.h>      // round
#include <string>      // strerror
#include <sys/stat.h>  // open
#include <sys/types.h> // open
#include <unistd.h>    // write
                       // udev_device_new_from_subsystem_sysname,
                       // udev_device_get_syspath,
                       // udev_device_get_sysattr_value
#include <QtCore>
#include <memory> // std::unique_ptr, std::make_unique

#include <drm/backlight.h>

// ============================================================================
// Backlight Implementation
// ============================================================================

void
udev_unref_wrap(udev *dev)
{
    (void) udev_unref(dev);
}

void
udev_device_unref_wrap(udev_device *dev)
{
    (void) udev_device_unref(dev);
}

backlight_t::backlight_t() : m_udev(udev_new(), udev_unref_wrap),
                             m_udev_device(udev_device_new_from_subsystem_sysname(m_udev.get(), "backlight", "intel_backlight"), udev_device_unref_wrap)
{
    if (m_udev == nullptr) {
        qWarning() << "Failed: udev_new";
        return;
    }

    if (m_udev_device == nullptr) {
        qWarning() << "Unable to find UDEV backlight node. Backlight support not available!!!";
        return;
    }

    m_max = value(value_t::MAX_BRIGHTNESS);
    m_level = value(value_t::BRIGHTNESS);

    qDebug() << "Maximum brightness level is" << m_max;
    qDebug().nospace() << "Initial brightness level is " << m_level << " (" << qPrintable(QString::number(m_level * 100.f / m_max, 'f', 3)) << "%)";
}

backlight_t::backlight_t(const uint32_t level) : backlight_t()
{
    set_level(level);
}

backlight_t::~backlight_t() {}

uint32_t
backlight_t::level() const
{
    float tmpf = m_level * 100.f / m_max;

    qDebug().nospace() << "Current brightness level is " << m_level << " (" << qPrintable(QString::number(tmpf, 'f', 3)) << "%)";

    return round(tmpf);
}

void
backlight_t::set_level(uint32_t level)
{
    if (m_udev_device == nullptr) {
        return;
    }

    qDebug() << "Setting level to" << level;

    level = std::min(level, 100u);
    level = std::max(level, 1u);
    level = round(level * m_max / 100.f);
    std::string str = std::to_string(level);

    std::string fullpath = std::string(udev_device_get_syspath(m_udev_device.get())) + "/brightness";
    int32_t fd = open(fullpath.c_str(), O_RDWR);
    if (fd < 0) {
        qWarning() << "Failed to open:" << fullpath.c_str() << "-" << strerror(errno);
        return;
    }

    uint32_t len1 = str.length();
    size_t len2 = write(fd, str.c_str(), len1);
    if (len1 != len2) {
        qWarning() << "Failed to write:" << fullpath.c_str() << "-" << level << "-" << strerror(errno);
        close(fd);
        return;
    }

    m_level = level;

    qDebug().nospace() << "Resulting brightness level is " << m_level << " (" << qPrintable(QString::number(m_level * 100.f / m_max, 'f', 3)) << "%)";

    close(fd);
}

void
backlight_t::increase(uint32_t step)
{
    step = std::min(step, 100u);
    step = std::max(step, 1u);
    uint32_t level = round(std::min((m_level * 100.f / m_max) + step, 100.f));

    qDebug() << "Increasing level by" << step;

    set_level(level);
}

void
backlight_t::decrease(uint32_t step)
{
    step = std::min(step, 100u);
    step = std::max(step, 1u);
    uint32_t level = round(std::max((m_level * 100.f / m_max) - step, 1.f));

    qDebug() << "Decreasing level by" << step;

    set_level(level);
}

uint32_t
backlight_t::value(const value_t v) const
{
    if (m_udev_device == nullptr) {
        return 0;
    }

    std::string path = to_string(v);
    QString valueStr = QString(udev_device_get_sysattr_value(m_udev_device.get(), path.c_str()));

    if (valueStr.isEmpty()) {
        qWarning() << "Failed: udev_device_get_sysattr_value -" << strerror(errno);
        return 0;
    }

    bool ok = false;
    uint32_t value = valueStr.toUInt(&ok, 10);

    if (!ok) {
        qWarning() << "Failed to convert value to uint:" << valueStr << "-" << strerror(errno);
        return 0;
    }

    return value;
}

std::string
backlight_t::to_string(const value_t v) const
{
    switch (v) {
        case value_t::BRIGHTNESS:
            return "brightness";
            break;
        case value_t::MAX_BRIGHTNESS:
            return "max_brightness";
            break;
            // omit default case to trigger compiler warning for missing cases
    }

    // We'll never get here unless the compiler warning is ignored.
    return "";
};
