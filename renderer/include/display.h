//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DISPLAY__H
#define DISPLAY__H

#include <glass_types.h>

#include <QObject>

// The following defines the maximum width and height for a display's
// framebuffer as well as a display's cursor buffer. Note that the maximum
// size of a cursor needs to be a factor of the maximum size of the display
// and the minimum size supported. The ratio for the cursor exists so that
// the host can scale the cursor when it need to scale the guest's framebuffer,
// similar to how a monitor woudl do this if the resolution that it sets is
// not equal to the resolution of the actual panel.
//
// Note that the ratio for the cursor was set to the width for the height.
// This is because for some reason, they want the width and height to match.
// And the width is likely to be the larger of the two.
#define MIN_DISPLAY_WIDTH 800
#define MIN_DISPLAY_HEIGHT 600
#define MAX_DISPLAY_WIDTH 3840
#define MAX_DISPLAY_HEIGHT 2160
#define MAX_DISPLAY_STRIDE (MAX_DISPLAY_WIDTH * 4)
#define MAX_CURSOR_WIDTH ((MAX_DISPLAY_WIDTH / MIN_DISPLAY_WIDTH) * 64)
#define MAX_CURSOR_HEIGHT ((MAX_DISPLAY_WIDTH / MIN_DISPLAY_WIDTH) * 64)
#define MAX_CURSOR_STRIDE (MAX_CURSOR_WIDTH * 4)

// The following defines the maximum number of display that we support in the
// display handler.
#define MAX_NUM_DISPLAYS (6)

// Note that if you change the above settings, you might need to fix the macro
// below so that you are allocating page aligned memory.
#define MAX_CURSOR_SIZE ((64 * 4) * 64)

// Note that if you change the above settings, you might need to fix the macro
// below so that you are allocating page aligned memory.
#define MAX_FRAMEBUFFER_SIZE ((MAX_DISPLAY_WIDTH * 4) * MAX_DISPLAY_HEIGHT)

// The following defines an invalid key for a display.
#define INVALID_KEY (-2)

class display_t : public QObject
{
    Q_OBJECT
public:
    display_t(json &display) { (void) display; }

    virtual ~display_t() = default;

    virtual const QString &owner(void) const = 0;
    virtual void set_owner(const QString &owner) = 0;

    virtual int32_t key(void) const = 0;
    virtual void set_key(int32_t key) = 0;

    virtual int32_t screen_id(void) const = 0;
    virtual void set_screen_id(int32_t screen_id) = 0;

    virtual bool primary(void) const = 0;
    virtual void set_primary(bool primary) = 0;

    virtual int32_t x(void) const = 0;
    virtual int32_t y(void) const = 0;
    virtual double x_f(void) const = 0;
    virtual double y_f(void) const = 0;
    virtual const QPoint &position(void) const = 0;
    virtual void set_position(const QPoint &position) = 0;

    virtual uint32_t width(void) const = 0;
    virtual uint32_t height(void) const = 0;
    virtual double width_f(void) const = 0;
    virtual double height_f(void) const = 0;
    virtual const QSize &resolution(void) = 0;
    virtual void set_resolution(const QSize &resolution) = 0;

    virtual const rect_t &frame(void) const = 0;
    virtual const rect_t &origin_frame(void) const = 0;
    virtual void set_frame(const rect_t &frame) = 0;

signals:
    void display_changed();

protected:
    QString m_owner;
    int32_t m_key;
    int32_t m_screen_id;
    bool m_primary;

    QPoint m_position;
    QSize m_resolution;

    rect_t m_frame;
    rect_t m_origin_frame;

private:
    Q_DISABLE_COPY(display_t);

    virtual void update_frame() = 0;
};

#endif // DISPLAY__H
