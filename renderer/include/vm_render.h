//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_RENDER__H
#define VM_RENDER__H

#include <glass_types.h>

#include <QObject>

#include <desktop_plane.h>
#include <render_source_plane.h>
#include <render_target_plane.h>

#include <vm_base.h>

class vm_render_t : public QObject
{
    Q_OBJECT
public:
    vm_render_t(std::shared_ptr<vm_base_t> base) : m_base(base)
    {
        qRegisterMetaType<rect_t>("rect_t");
        qRegisterMetaType<uint32_t>("uint32_t");
        qRegisterMetaType<uuid_t>("uuid_t");
        qRegisterMetaType<point_t>("point_t");
        qRegisterMetaType<std::shared_ptr<QImage>>("std::shared_ptr<QImage>");
        qRegisterMetaType<std::shared_ptr<cursor_t>>("std::shared_ptr<cursor_t>");
        qRegisterMetaType<std::shared_ptr<render_source_plane_t>>("std::shared_ptr<render_source_plane_t>");
    }

    virtual ~vm_render_t() = default;

    virtual uuid_t uuid()
    {
        if (m_base) {
            return m_base->uuid();
        } else {
            return uuid_t();
        }
    }

    virtual std::shared_ptr<vm_base_t> base() { return m_base; }

    bool blanked() { return m_blanked; }

public slots:
    virtual void update_render_targets(desktop_plane_t *desktop, uuid_t uuid) = 0;

signals:
    void dirty_rect(rect_t rect);

    void move_cursor_signal(uuid_t uuid, window_key_t key, point_t point);
    void show_cursor_signal(uuid_t uuid, uint32_t key, point_t point, std::shared_ptr<cursor_t> cursor);
    void hide_cursor_signal(uuid_t uuid, uint32_t key);
    void restore_qemu_signal();

protected:
    std::shared_ptr<vm_base_t> m_base;
    bool m_blanked = false;
};

#endif // VM_RENDER__H
