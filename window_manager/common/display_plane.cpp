//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <display_plane.h>

display_plane_t::display_plane_t(rect_t rect,
                                 point_t plane_origin,
                                 uint32_t banner_height,
                                 std::shared_ptr<plane_t> parent) : plane_t(rect, plane_origin, parent),
                                                                    m_render_plane(rect_t(0, 0, rect.width(), rect.height() - banner_height),
                                                                                   point_t(0, banner_height)),
                                                                    m_fb(nullptr),
                                                                    m_cursor(nullptr),
                                                                    m_banner_height(banner_height),
                                                                    m_name("")

{
}

render_plane_t &
display_plane_t::render_plane()
{
    return m_render_plane;
}

transform_t
display_plane_t::from(plane_t &source_plane)
{
    return translate(source_plane);
}

transform_t
display_plane_t::to(plane_t &source_plane)
{
    return from(source_plane).inverted();
}

transform_t
display_plane_t::translate(plane_t &source_plane)
{
    transform_t translate_transform;
    qreal sx = source_plane.origin().x();
    qreal sy = source_plane.origin().y();

    translate_transform.translate(sx, sy);

    return translate_transform;
}

point_t
display_plane_t::map_to(plane_t *source_plane, point_t point)
{
    if (!source_plane) {
        return point_t(0, 0);
    }

    return to(*source_plane).map(point);
}

point_t
display_plane_t::map_from(plane_t *source_plane, point_t point)
{
    Expects(source_plane);

    return from(*source_plane).map(point);
}

uint32_t
display_plane_t::unique_id()
{
    return m_unique_id;
}

void
display_plane_t::set_unique_id(uint32_t unique_id)
{
    m_unique_id = unique_id;
}

void
display_plane_t::set_rect(rect_t rect)
{
    m_plane &= rect_t();
    m_plane += rect;

    m_render_plane.set_origin(point_t(0, m_banner_height));
    m_render_plane.set_rect(rect_t(0, 0, rect.width(), rect.height() - m_banner_height));

    m_scratch = std::make_shared<QImage>(rect.size(), QImage::Format_RGB32);
    m_scratch->fill(Qt::black);
}

uint32_t
display_plane_t::banner_height()
{
    return m_banner_height;
}

void
display_plane_t::operator+=(const rect_t &rect)
{
    set_rect(rect);
}

std::string
display_plane_t::name()
{
    return m_name;
}

void
display_plane_t::set_name(std::string name)
{
    m_name = name;
}

std::shared_ptr<framebuffer_t>
display_plane_t::framebuffer()
{
    return m_fb;
}
