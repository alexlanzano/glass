//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <screenshotaction.h>

screenshot_action_t::screenshot_action_t() {}

screenshot_action_t::~screenshot_action_t() {}

void
screenshot_action_t::operator()()
{
// Only allow screenshots on debug builds
#ifndef DEBUG_DISABLE
    emit save_screenshot();
#endif
}
