//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_VM_INPUT__H
#define PV_VM_INPUT__H

#include <vm_base.h>
#include <vm_input.h>
#include <window_manager.h>

#include <guestinputsink.h>
#include <xt_input_global.h>

#include <sinks/qemu2sink.h>
#include <sinks/vkbdsink.h>

class pv_vm_input_t : public vm_input_t
{
    Q_OBJECT
public:
    // Constructor
    pv_vm_input_t(std::shared_ptr<vm_base_t> vm, window_manager_t &wm);
    virtual ~pv_vm_input_t();

    void transform_event(xt_input_event *event);

    virtual void wake_up_toggle();

public slots:
    // Receive the event
    virtual void input_event_slot(xt_input_event event);

    // If a guest loses focus, we should release any currently held
    // keys and mouse buttons
    virtual void lost_focus();

    // If a guest gets focus, we need to display any
    // current led codes
    virtual void got_focus();

    void vkbd_connection_status(bool is_connected);
    virtual void keyboard_led_changed(const uint32_t led_code);

private:
    void forward_to_active_sinks(const xt_input_event &event);
    KeyMask m_key_state;

    point_t m_last_hotspot;
    point_t m_last_guest_hotspot;

    window_manager_t &m_wm;

    // What the currently selected guestSink is
    std::shared_ptr<guest_input_sink_t> m_active_sink;

    // The guest's QEMU sink.
    std::shared_ptr<qemu2_sink_t> m_qemu2_sink;

    // The guest's VKBD sink, if supported.
    std::shared_ptr<vkbd_sink_t> m_vkbd_sink;

    // A list of available sinks for the current guest,
    // excluding the debug sink, which is only compiled
    // in for debugging.
    list_t<std::shared_ptr<guest_input_sink_t>> m_sinks;

    uint32_t m_led_code;
};

#endif
