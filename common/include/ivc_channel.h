//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef IVC_CHANNEL__H
#define IVC_CHANNEL__H

#include <QObject>
#include <functional>
#include <glass_types.h>
#include <libivc.h>
#include <unistd.h>

class ivc_connection_t : public QObject
{
    Q_OBJECT
public:
    static void event_callback(void *opaque, struct libivc_client *client)
    {
        (void) client;
        ivc_connection_t *self = static_cast<ivc_connection_t *>(opaque);
        if (!self) {
            return;
        }

        QMutexLocker locker(&self->m_lock);

        QMetaObject::invokeMethod(self, "process_event", Qt::QueuedConnection);
    }

    static void disconnect_callback(void *opaque, struct libivc_client *client)
    {
        (void) client;
        ivc_connection_t *self = static_cast<ivc_connection_t *>(opaque);
        if (!self) {
            return;
        }

        QMutexLocker locker(&self->m_lock);

        QMetaObject::invokeMethod(self, "process_disconnect", Qt::QueuedConnection);
        vg_debug() << "Got an ivc disconnect";
    }

    // Server side connection
    ivc_connection_t(struct libivc_client *client,
                     std::function<void(struct libivc_client *)> event_callback = nullptr,
                     std::function<void(struct libivc_client *)> disconnect_callback = nullptr) : m_connected(true),
                                                                                                  m_event_callback(event_callback),
                                                                                                  m_disconnect_callback(disconnect_callback),
                                                                                                  m_client(client)
    {
        if (!libivc_isOpen(m_client)) {
            throw std::logic_error("Can't construct a connection with a closed client");
        }

        libivc_register_event_callbacks(client,
                                        &ivc_connection_t::event_callback,
                                        &ivc_connection_t::disconnect_callback,
                                        this);
    }

    ~ivc_connection_t()
    {
        if (m_connected && m_client && libivc_isOpen(m_client)) {
            libivc_disconnect(m_client);
            m_connected = false;
            m_client = nullptr;
        }
    }

    void disconnect()
    {
        if (m_connected && m_client && libivc_isOpen(m_client)) {
            libivc_disconnect(m_client);
            m_connected = false;
            m_client = nullptr;
        }
    }

    bool connected() { return m_connected; }
    void enable_events()
    {
        QMutexLocker locker(&m_lock);
        libivc_enable_events(m_client);
    }

    void disable_events()
    {
        QMutexLocker locker(&m_lock);
        libivc_disable_events(m_client);
    }

    struct libivc_client *client() { return m_client; }

    void register_event_handler(std::function<void(struct libivc_client *)> callback)
    {
        QMutexLocker locker(&m_lock);

        m_event_callback = callback;
    }

    void register_disconnect_handler(std::function<void(struct libivc_client *)> callback)
    {
        QMutexLocker locker(&m_lock);

        m_disconnect_callback = callback;
    }

private slots:
    void process_event()
    {
        if (m_client && m_event_callback && m_connected) {
            m_event_callback(m_client);
        }
    }

    void process_disconnect()
    {
        if (m_client && m_disconnect_callback && m_connected) {
            m_disconnect_callback(m_client);
        }
    }

private:
    qmutex_t m_lock;
    bool m_connected{false};
    std::function<void(struct libivc_client *)> m_event_callback{nullptr};
    std::function<void(struct libivc_client *)> m_disconnect_callback{nullptr};

    struct libivc_client *m_client{nullptr};
};

class ivc_server_t : public QObject
{
    Q_OBJECT
public:
    static void connect_callback(void *opaque, struct libivc_client *client)
    {
        ivc_server_t *self = static_cast<ivc_server_t *>(opaque);
        if (!self) {
            return;
        }

        QMutexLocker locker(&self->m_lock);
        QMetaObject::invokeMethod(self, "new_client", Qt::QueuedConnection, Q_ARG(void *, client));
    }

    // Client side connection, probably not used in dom0
    ivc_server_t(domid_t domid,
                 uint16_t port) : m_domid(domid),
                                  m_port(port)
    {
        int rc = 0;

        rc = libivc_start_listening_server(&m_server,
                                           m_port,
                                           m_domid,
                                           LIBIVC_ID_ANY,
                                           &ivc_server_t::connect_callback,
                                           this);

        if (rc != SUCCESS) {
            vg_debug() << "Failed to listen on " << m_domid << m_port;
            throw std::logic_error("Couldn't start listening ivc server!");
        } else {
            m_listening = true;
        }
    }

    ~ivc_server_t()
    {
        if (m_listening && m_server) {
            libivc_shutdownIvcServer(m_server);
            m_server = nullptr;
            m_listening = false;
        }
    }

private slots:
    void new_client(void *client)
    {
        struct libivc_client *c = reinterpret_cast<struct libivc_client *>(client);

        if (!c) {
            return;
        }

        emit client_ready(c);
    }

signals:
    void client_ready(void *client);

private:
    qmutex_t m_lock;
    bool m_listening{false};
    domid_t m_domid{0};
    uint16_t m_port{0};

    struct libivc_server *m_server{nullptr};
};

#endif // IVC_CHANNEL__H
