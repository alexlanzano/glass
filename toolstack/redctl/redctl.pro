QT       += core

include(../../common_include.prx)

CONFIG += c++14

TARGET = redctl_toolstack

TEMPLATE = lib
DESTDIR = $$VG_BASE_DIR/lib

SOURCES += redctl.cpp 

HEADERS += redctl.h
HEADERS += ../include/toolstack.h
HEADERS += ../include/vm.h
HEADERS += ../include/vm_base.h

INCLUDEPATH += "../include"

target.path = /usr/lib
INSTALLS += target
